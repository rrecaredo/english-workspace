﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.English.Workspace.Model
{
    public abstract class BaseEntity : IEntity<Guid>
    {
        public Guid ID { get; set; }

        public BaseEntity()
        {
            ID = Guid.NewGuid();
        }
    }
}