﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace My.English.Workspace.Model
{
    interface IEntity<T>
    {
        T ID { get; set; }
    }
}
