﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(My.English.Workspace.UI.Services.Startup))]
namespace My.English.Workspace.UI.Services
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
