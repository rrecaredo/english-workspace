﻿using System.Web;
using System.Web.Mvc;

namespace My.English.Workspace.UI.Services
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
