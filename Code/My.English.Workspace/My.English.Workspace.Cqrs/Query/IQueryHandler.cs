﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.English.Workspace.Cqrs
{
    public interface IQueryHandler<in TParameter, out TResult>
        where TResult : IQueryResult
        where TParameter : IQuery
    {
        TResult Retrieve(TParameter query);
    }
}
