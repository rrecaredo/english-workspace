﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace My.English.Workspace.Cqrs
{
    public class QueryBus : IQueryBus
    {
        public TResult Submit<TParameter, TResult>(TParameter query)
            where TParameter : IQuery
            where TResult : IQueryResult
        {
            var handler = DependencyResolver.Current.GetService<IQueryHandler<TParameter, TResult>>();
            return handler.Retrieve(query);
        }

    }
}
