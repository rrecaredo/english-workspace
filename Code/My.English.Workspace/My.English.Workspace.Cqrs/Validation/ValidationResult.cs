﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.English.Workspace.Cqrs
{
    public class ValidationResult
    {
        public ValidationResult() { }

        public ValidationResult(string memberName, string message)
        {
            this.MemberName = memberName;
            this.Message = message;
        }

        public ValidationResult(string message)
        {
            this.Message = message;
        }

        public string MemberName { get; set; }
        public string Message { get; set; }
    }
}
