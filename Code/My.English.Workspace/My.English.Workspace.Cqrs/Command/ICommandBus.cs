﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace My.English.Workspace.Cqrs
{
    public interface ICommandBus
    {
        ICommandResult Submit<TCommand>(TCommand command)
            where TCommand : ICommand;

        IEnumerable<ValidationResult> Validate<TCommand>(TCommand command)
            where TCommand : ICommand;
    }
}
